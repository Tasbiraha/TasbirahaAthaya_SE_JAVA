
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;
import java.util.*;

public class AddProducts extends HttpServlet {
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        /*PrintWriter out = res.getWriter();
        RequestDispatcher rdisp = req.getRequestDispatcher("/addcomp.html?e=1");
        RequestDispatcher rdispsuc = req.getRequestDispatcher("/addsuc.html");*/
        
        String name = "";
        int price=0, prcnt=0, prtype=0;
        try {
            name = req.getParameter("pname");
            price = Integer.parseInt(req.getParameter("pprice"));
            prcnt = Integer.parseInt(req.getParameter("prof"));
            prtype = Integer.parseInt(req.getParameter("ptype"));
        } catch (Exception e) {
            res.sendRedirect("./addcomp.html?e=1");
            return;
        }
        
        if(prcnt > 100 || price <= 0 || prtype < 1 || prtype > 3)
        {
            res.sendRedirect("./addcomp.html?e=2");
            return;
        }
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection cnc = DriverManager.getConnection("jdbc:mysql://db4free.net/tasbiraha?useSSL=false", "athaya", "tasbiraha");
            PreparedStatement ps = cnc.prepareStatement("INSERT INTO ProductTable VALUES (NULL,?,?,?,?)");
            ps.setString(1, name);
            ps.setInt(2, price);
            ps.setInt(3, prcnt);
            ps.setInt(4, prtype);
            int stat = ps.executeUpdate();
            if(stat == 0)
            {res.sendRedirect("./addcomp.html?e=4");
                return;}
            else
            {res.sendRedirect("./addsuc.html");
                return;}
        } catch (Exception e) {
            res.sendRedirect("./addcomp.html?e=3");
            return;
        }
        
        
    }
}
