<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>About</title>
    </head>
    
<!-----------------------CSS for nav bar------------------>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 12px 14px;
  text-decoration: none;
  font-size: 14px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}
</style>
<!------CSS for nav bar Ends---->

    
    <body>
        <% if (session.getAttribute("uname") == null) {%>
        <!---------navbar--------->
      <div class="topnav">
        <a class="active" href="about.jsp">About</a>
        <a href="login.jsp">login</a>       
      </div>
        <br>
        
        <% }else {%>
       <div class="topnav">
        <a href="dashboard.jsp">Dashboard</a>
        <a href="addcomp.html">Add Products</a>
        <a href="updateshow.jsp">Update Products</a>
        <a href="delprod.jsp">Delete Products</a>
        <a class="active" href="about.jsp">About</a>
        <a href="#">logged in as <%=session.getAttribute("uname")%></a>
        <a href="logout.jsp">Logout</a>
      </div>
        <br>
        <%}%>
  <!---------navbar Ends--------->
  
        <h1>It is an e-commerce website</h1>
        <h2>Developed by: Tasbiraha Athaya</h2>
    </body>
</html>
