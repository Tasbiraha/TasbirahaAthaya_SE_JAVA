<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
 
<html>
   <head>
      <title>DashBoard</title>
   </head>
  <!-----------------------CSS for nav bar------------------>
    <style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 13px 14px;
  text-decoration: none;
  font-size: 14px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}
</style>
<!------CSS for nav bar Ends---->

    <body>
        <!---------navbar--------->
      <div class="topnav">
        <a class="active" href="dashboard.jsp">Dashboard</a>
        <a href="addcomp.html">Add Products</a>
        <a href="updateshow.jsp">Update Products</a>
        <a href="delprod.jsp">Delete Products</a>
        <a href="about.jsp">About</a>
        <a href="#">Logged in</a>
        <a href="logout.jsp">Logout</a>
      </div>
        <br>
  <!---------navbar Ends--------->
     <sql:setDataSource var = "snap" driver = "com.mysql.jdbc.Driver"
         url = "jdbc:mysql://db4free.net/tasbiraha?useSSL=false"
         user = "athaya"  password = "tasbiraha"/>
 
      <sql:query dataSource = "${snap}" var = "result">
         SELECT * from ProductTable ORDER BY prcntPrft DESC;
      </sql:query>
 
      <table border = "1" width = "100%">
         <tr>
            <th>Name</th>
            <th>%Profit</th>
         </tr>
         
         <c:forEach var = "row" items = "${result.rows}">
            <tr>
               <td><c:out value = "${row.Name}"/></td>
               <td><c:out value = "${row.prcntPrft}"/></td>
            </tr>
         </c:forEach>
      </table>
 
   </body>
</html>
